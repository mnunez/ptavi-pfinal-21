#!/usr/bin/python3
# -*- coding: utf-8 -*-

import socketserver
import socket
import sys
import threading
import time
import simplertp

class SIPHandler(socketserver.DatagramRequestHandler):
    """
    UDP sip handler class
    """

    def handle(self):
      
        #Lectura de cada línea de lo que envía el cliente que llega vía ServidorSip
        msg = self.rfile.read().decode('utf-8') #decodificación
        #metodos + dirección + sip
        sip = msg.split()[2] 
        metodos = msg.split()[0]
        direccion = msg.split()[1]
        
        #si el método de la petición que realiza el cliente es INVITE, ACK ó BYE
        #comprueba SIP/2.0  
        if metodos.upper() == 'INVITE' or 'ACK' or 'BYE':
            if sip != 'SIP/2.0':
                self.wfile.write(b"SIP/2.0 400 Bad Request")

            if sip == 'SIP/2.0':
                #INVITE --> SIP/2.0 200 OK
                if metodos.upper() == 'INVITE':
                    self.wfile.write(b"SIP/2.0 200 OK\r\n\r\n")
                    date = time.strftime('%Y%m%d%H%M%S', time.gmtime(time.time() + 3600))
                    ip = self.client_address[0]
                    port = self.client_address[1]
                    print(date + ' ' + 'SIP from' + ' ' + ip + ':' + str(port) + ': INVITE.\n')
                    print(date + ' ' + 'SIP to' + ' ' + ip + ':' + str(port) + ': SIP/2.0 200 OK' + '.' + '\n')
                 
                if metodos.upper() == 'ACK': 
                    #ACK --> SIP/2.0 200 OK
                    self.wfile.write(b"SIP/2.0 200 OK\r\n\r\n")
                    #Envío de paquetes RTP
                    FICHERO = sys.argv[2]
                    IP = sys.argv[1].split(':')[0]
                    PORT = int(sys.argv[1].split(':')[1])
                    sender = simplertp.RTPSender(ip=IP, port=PORT, file=FICHERO, printout=True)
                    sender.send_threaded()
                    time.sleep(2)
                    print("Ending send thread.\n")
                    sender.finish()


                elif metodos.upper() == 'BYE':
                    #BYE --> SIP/2.0 200 OK
                    self.wfile.write(b"SIP/2.0 200 OK\r\n\r\n")
                    ip = self.client_address[0]
                    port = self.client_address[1]
                    date = time.strftime('%Y%m%d%H%M%S', time.gmtime(time.time() + 3600))
                    print(date + ' ' + 'SIP to' + ' ' + ip + ':' + str(port) + ': SIP/2.0 200 OK' + '.' + '\n')
                    

            
def main():
    #Creamos servidor de sip y escuchamos
    try:
        IP = sys.argv[1].split(':')[0] #IP Servidor SIP
        PORT = int(sys.argv[1].split(':')[1]) #puerto donde escucha Servidor SIP
        FICHERO = sys.argv[2] #nombre fichero
    except IndexError:
        sys.exit("Usage: python3 serverrtp.py <IPServerSIP>:<portServerSIP> <file>")
    #dirección SIP del Servidor RTP es el nombre del fichero sin '.mp3'
    DIRECC = FICHERO.split('.')[0] + '@signasong.net'
    try: #socket: envía petición de registro a Servidor SIP
        with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as my_socket:
            my_socket.bind(('', 6002)) #puerto al que pertenece
            my_socket.connect((IP, PORT))
            SIP = ('REGISTER' + ' ' 'sip:' + str(DIRECC) +
            ' ' + 'SIP/2.0\r\n\r\n')
            date = time.strftime('%Y%m%d%H%M%S', time.gmtime(time.time() + 3600))
            print(date + ' ' + 'SIP to' + ' ' + IP + ':' + str(PORT) + ':', SIP + '.' + '\n')
            my_socket.send(bytes(SIP, 'utf-8'))
            data = my_socket.recv(1024).decode('utf-8')
            print('Received REGISTER.\n')

    except ConnectionRefusedError:
        print("Error connecting to server.")


    try:
        serv = socketserver.UDPServer(('', 6002), SIPHandler)
        print("Listening...\n")
              
    except OSError as e:
        sys.exit(f"Error starting to listen: {e.args[1]}.")

    try:
        serv.serve_forever()
    except KeyboardInterrupt:
        print("Ending server.")
        sys.exit(0)
        


if __name__ == "__main__":
    main()
             
        
