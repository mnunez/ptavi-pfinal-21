#!/usr/bin/python3
# -*- coding: utf-8 -*-

import socketserver
import socket
import sys
import threading
import time
import simplertp

class RTPHandler(socketserver.BaseRequestHandler):
    """Clase para manejar paquetes RTP que lleguen"""

    def handle(self):
        msg = self.request[0]
        # sólo nos interesa lo que hay a partir del byte 54 del paquete UDP
        # que es donde está la payload del paquete RTP qeu va dentro de él
        payload = msg[12:]
        # Escribimos esta payload en el fichero de salida
        self.output.write(payload)
        print("Received.")

    @classmethod
    def open_output(cls, filename):
        """Abrir el fichero donde se va a escribir lo recibido
        Lo abrimos en modo escritura (w) y binario (b)
        Es un método de la clase para que podamos invocarlo sobre la clase
        antes de empezar a recibir paquetes."""

        cls.output = open(filename, 'wb')

    @classmethod
    def close_output(cls):
        """Cerrar el fichero donde se va a escribir lo recibido
        Es un método de la clase para que podamos invocarlo sobre la clase
        después de terminar de recibir paquetes."""

        cls.output.close()



def main():

    """
    Parámetros que introduce el cliente
    """
    try:
        IP = sys.argv[1].split(':')[0] #IP Servidor SIP
        PORT = int(sys.argv[1].split(':')[1]) #puerto donde escucha Servidor SIP
        DIR_CLIENT = sys.argv[2].split(':')[1] #dirección SIP del cliente
        DIR_SERV = sys.argv[3].split(':')[1] #dirección SIP del Servidor RTP
        TIME = int(sys.argv[4]) #tiempo
        FICHERO = sys.argv[5] #nombre del fichero
    except IndexError: #si no se introducen los parámetros correctos --> Error
        sys.exit("Usage: python3 client.py <IPServerSIP>:<portServerSIP> sip:<addrClient> sip:<addrServerRTP> <time> <file>")


    
    try: #socket: el cliente envía peticiones
        with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as my_socket:
            my_socket.connect((IP, PORT))
            #se almacena en la variable 'date' la fecha y hora en momento actual
            date = time.strftime('%Y%m%d%H%M%S', time.gmtime(time.time() + 3600))
            #se imprime por pantalla 'Starting...' indicando el comienzo 
            print(date + ' ' + 'Starting...' + '\n')
            #contenido a enviar o peticiones a enviar (REGISTER, INVITE, ACK, BYE)
            #tras la petición ACK, recepción de paquetes RTP
            SIP = ('REGISTER' + ' ' 'sip:' + str(DIR_CLIENT) +
            ' ' + 'SIP/2.0\r\n\r\n')
            print(date + ' ' + 'SIP to' + ' ' + IP + ':' + str(PORT) + ':', SIP + '.' + '\n')
            my_socket.send(bytes(SIP, 'utf-8'))
            data = my_socket.recv(1024).decode('utf-8')
            print('Received REGISTER.\n')


            print(date + ' ' + 'SIP from' + ' ' + IP + ':' + str(PORT) + ': SIP/2.0 200 OK.' + '\n')
            SIP_1 = ('INVITE' + ' ' 'sip:' + str(DIR_SERV) +
                ' ' + 'SIP/2.0\r\n\r\n')
            print(date + ' ' + 'SIP to' + ' ' + IP + ':' + str(PORT) + ':', SIP_1 + '.' + '\n')
            #body de la petición
            #cabecera de tamaño (adicional)
            tamaño_cuerpo = sys.getsizeof("v = 0" + '\r\n' + "o = sip:" + DIR_CLIENT + IP + '\r\n' + "s = "  + DIR_SERV.split('@')[0] + '\r\n' + "t = 0" '\r\n' + "m = audio " + str(PORT) + " RTP + '\r\n'")
            my_socket.send(bytes(SIP_1, 'utf-8') + bytes("Content-Type:   application/sdp\r\n", 'utf-8') + bytes("Content-Length: " + str(tamaño_cuerpo) + '\r\n', 'utf-8') + bytes("v = 0\r\n", 'utf-8') + bytes("o = sip:" + DIR_CLIENT + IP + "\r\n", 'utf-8') + bytes("s = " + DIR_SERV.split('@')[0] + "\r\n", 'utf-8') + bytes("t = 0", 'utf-8') + bytes("m = audio" + str(PORT) + "RTP\r\n", 'utf-8'))
            data_1 = my_socket.recv(1024).decode('utf-8')
            print('Received INVITE.\n')
            
            
            print(date + ' ' + 'SIP from' + ' ' + IP + ':' + str(PORT) + ': SIP/2.0 200 OK' + '.' + '\n')
            ACK = ('ACK' + ' ' 'sip:' + str(DIR_SERV) + ' ' + 'SIP/2.0\r\n\r\n')
            print(date + ' ' + 'SIP to' + ' ' + IP + ':' + str(PORT) + ':', ACK + '.' + '\n')
            my_socket.send(bytes(ACK, 'utf-8'))
            data_2 = my_socket.recv(1024).decode('utf-8')
            
            #recepción de paquetes RTP
            # Abrimos un fichero para escribir los datos que se reciban
            RTPHandler.open_output('output.mp3')
            # Creamos el socket, lo configuramos y lo atamos a un servidor/puerto
            serv = socketserver.UDPServer(('0.0.0.0', 0), RTPHandler)
            print("Listening...\n")
            # El bucle de recepción (serv_forever) va en un hilo aparte,
            # para que se continue ejecutando este programa principal,
            # y podamos interrumpir ese bucle más adelante
            threading.Thread(target=serv.serve_forever).start()
            # Paramos unos segundos.
            time.sleep(10)
            print("Time passed, shutting down receiver loop.\n")
            # Paramos el bucle de recepción, con lo que terminará el thread,
            # y dejamos de recibir paquetes
            serv.shutdown()
            # Cerramos el fichero donde estamos escribiendo los datos recibidos
            RTPHandler.close_output()

            
            SIP = ('BYE' + ' ' 'sip:' + str(DIR_SERV) +
            ' ' + 'SIP/2.0\r\n\r\n')
            print(date + ' ' + 'SIP to' + ' ' + IP + ':' + str(PORT) + ':', SIP + '.' + '\n')
            my_socket.send(bytes(SIP, 'utf-8'))
            data_3 = my_socket.recv(1024).decode('utf-8')
            print('Received BYE.\n')
            print('Ending...\n')

            #Cliente terminado
            print("Finished client.")

    except ConnectionRefusedError: 
        print("Error connecting to server.")




if __name__ == "__main__":
    main()
