## Parte básica

    * En la práctica realizada se envían paquetes RTP desde un servidor RTP a un cliente a través de un servidor SIP.
	* Duda surgida durante la práctica: obtener archivos en formato '.mp3'.
	* Históricos mostrados por pantalla en el terminal (podrían recopilarse en un fichero.)
	
	

## Parte adicional 

     * Cabecera de tamaño: 
	   Se ha incluido en los paquetes SIP que tienen cuerpo. Concretamente en los paquetes con método "INVITE" que envía el cliente.
	   Se ha utilizado "sys.getsizeof" con el fin de obtener el tamaño de la cabecera la cual indica: versión, originador e identificador de sesión, nombre de la sesión, tiempo que lleva la sesión activa y tipo de elemento multimedia, puerto de escucha y protocolo de transporte utilizados. 
