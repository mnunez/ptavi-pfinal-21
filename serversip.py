#!/usr/bin/python3
# -*- coding: utf-8 -*-

import socketserver
import socket
import sys
import time
import json


class SIPRegisterHandle(socketserver.DatagramRequestHandler):
    """
    UDP echo handler class
    """
    #diccionario vacío
    dicc = {}
    
    
    def handle(self):
        #fichero '.json'
        self.json2register()
        msg = self.rfile.read().decode('ISO-8859-1') #decodificación
        metodos = msg.split()[0]
        direccion = msg.split()[1]
        sender_ip = self.client_address[0]
        sender_port = self.client_address[1]
        date = time.strftime('%Y%m%d%H%M%S', time.gmtime(time.time() + 3600))
        #si REGISTER, se registran las direcciones en el diccionario
        if metodos == 'REGISTER':
            print(date + ' ' + 'SIP from' + ' ' + sender_ip + ':' + str(sender_port) + ':', 'REGISTER' + ' ' 'sip:' + str(direccion) + ' ' + 'SIP/2.0.\r\n\r\n')
            #si dirección no está en el diccionario
            if direccion not in self.dicc:
                #diccionario con claves direcciones SIP y valores IP y puerto de origen
                self.dicc[direccion] = (sender_ip, sender_port)
                self.wfile.write(b"SIP/2.0 200 OK\r\n\r\n")
                print(date + ' ' + 'SIP to' + ' ' + sender_ip + ':' + str(sender_port) + ': SIP/2.0 200 OK.\n')
                
        #si INVITE, ACK ó BYE se envía el correspondiente mensaje
        elif metodos == 'INVITE' or 'ACK' or 'BYE':
            user = msg.split()[1]
            if direccion in self.dicc:
                self.send(msg, user)
        self.register2json()

    #función de envío de mensaje a IP y puerto de origen
    def send(self, msg, user):        
        ip = self.dicc[user][0]
        port = self.dicc[user][1]
        #Reenvío
        try:
            with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as my_socket:
                my_socket.connect((ip, port))
                my_socket.send(msg.encode('utf-8'))
                data = my_socket.recv(1024).decode('utf-8')
                print('Sent.\n')

        except ConnectionRefusedError:
            print("Error connecting to server.")

            

    #Función que abre fichero 'registrar.json'
    def register2json(self):
        with open('registrar.json', 'w', encoding='utf-8') as json_file:
            json.dump(self.dicc, json_file, indent=3)

    #Función que escribe direcciones con IP y puerto de origen en el fichero 'registrar.json'
    def json2register(self):
        try:
            with open('registrar.json', 'r', encoding='utf-8') as json_file:
                self.dicc = json.load(json_file)
        except:
            self.dicc = {}
             
                   
        

def main():
    #Creamos servidor y escuchamos
    try:
        Port = int(sys.argv[1])
    except IndexError:
        sys.exit("Usage: python3 serversip.py <port>")

    try:
        serv = socketserver.UDPServer(('', Port), SIPRegisterHandle)
        print("Listening...\n")
    except OSError as e:
        sys.exit(f"Error starting to listen: {e.args[1]}.")

    try:
        serv.serve_forever()
    except KeyboardInterrupt:
        print("Ending server.")
        sys.exit(0)
        


if __name__ == "__main__":
    main()
